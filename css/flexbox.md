# Flexbox

La propriété `flex` est une propriété 
raccourcie qui définit la capacité d'un élément flexible à modifier ses dimensions afin de remplir l'espace disponible de son conteneur 
## Synstaxe


```css
    diplay: flex ;
    flex-direction: ;
    flex-grow: ;
    flex-wrap: ;
    flex-shrink: ;
    flex-basis: ;
```

## Flex-Direction
    
`flex-direction` définit la façon dont les éléments flexibles sont placés dans un conteneur flexible
```
    .container
```
##  Flex-grow

`flex-grow` définit le facteur d'expansion d'un élément flexible selon sa dimension principale


##  Flex-wrap

`flex-wrap` indique si les éléments flexibles sont contraints à être disposés sur une seule ligne ou s'ils peuvent être affichés sur plusieurs lignes avec un retour automatique

## Flex-shrink

`flex-shrink`définit le facteur de rétrécissement d'un élément flexible

## Flex-basis

`flex-basis` détermine la base de flexibilité utilisée comme taille initiale principale pour élément flexible