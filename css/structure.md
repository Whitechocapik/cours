# Structuration du Css

```css
body {
    color: blue;
}
```

## Sélécteur

`p {}` = pour une balise htlm

`.nomDeLaClass {}` = pour un attribut class

`#leNomDeLId {}` = pour un attribut id

## Le modèle de boîte

`padding` = pour les marges intérieures

`margin` = pour les marges extérieures 

`border` = petit liseré

## Police & Texte

`font-size: ;` = pour regler la taille des lettres en px

`line-height: ;` = la hauteur de ligne

`letter-spaccing: ;` = l'espace entre les lettres

`font-family: ; ` = permet de choisir une police

`text-align: ;` = spécifie l'alignement d'un texte

`overflow-wrap: ;` = permet de couper les mots trop longtemps

`font-weight: ;` = permet de spécifier le graissage de la police 


## Positionnement

permet de spécifier la position de l'élément
```css
    top : ;
    left : ;
    right : ;
    bottom : ;
 ```

## background

`background-color: ;` = permet de mettre un fond de couleur

`background-image: ;` = permet de mettre un fond de couleur à une image

`background-repeat: ;` = définit la façon dont les images utilisées en arrière-plan sont répétées

`background-position: ;` = permet de spécifier la position de l'élément
    
