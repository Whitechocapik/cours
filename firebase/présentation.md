# Firebase

## Crée un projet

1)   Allez sur le site de [firebase](https://firebase.google.com/)

2)  Vous cliquez acceder à la console

3) Ajouter un Projet puis donner lui un nom

 ## Deploy votre projet
 
* installer les tools de firebase `npm i -g firebase-tools`

  `i` = `install`,
 
  `-g` = géneral
 
* `firebase init hosting` puis dans hosting clique `Use an existing project`
 
*  mais avant ça il faut vous connecter à firebase en faisant la commande `firebase login`
 
  
```
    What do you want to use as your public directory?./
    Configure as a single-page app (rewrite all urls to /index.html)? No
    File .//404.html already exists. Overwrite? No
    File .//index.html already exists. Overwrite? No
             
```
Puis mettre `firebase deploy`

