# Gitignore

## Enlever des fichiers

1) Crée un fichier `.gitignore` 
2) Taper dans le `.gitignore` le nom des fichiers que tu veux supprimer 

## Supprimer des fichiers qui dans le gitlab

1) Aller dans `le terminal`  
2) Taper `git rm -rf leNomDuFichier`
