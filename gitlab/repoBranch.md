# Repo
  
## Créer un repo
 
 
 1) Ecrire sur le terminal `git init`
 
 2) `clique droit + add` pour ajouter un fichier sur Gitlab 
 
 3) Cliquer sur `la flèche verte` pour `Commit`
 
 4) Décocher `Perform code analysis` et `check TODO`
 
 5) Cliquer sur le `Commit and Push`
 
 6) Après avoir finit de créer son projet Gitlab,
  cliquer dans Gitlab clone puis copier le Clone with HTTPS
  
  7) Puis dans Define remote coller l'Url et cliquer sur Push
    
 
## Créer un projet Gitlab
 
1) Cliquer sur `New Project`
 
2) Cliquer sur `Blank Project`
 
3) Vous mettez un nom à se projet
 
4) Cliquer sur `Create Project`
 
## Importer un repo
  
1) Cliquer `clone` sur le projet que vous voulez prendre puis prendre son `Url`
  
2) Aller dans `VCS` Cliquer sur `Get from version control` puis vous coller l'`Url`
  
## Modifier le repo
 
1) Modifier le code 
 
2) Cliquer sur `la flèche verte`  
 
3) Puis cliquer sur `Commit and Push`
 
 
 ## Branch
 
   Branch sert a crée une copie du repo 
   a une dater et le modifier sans que sa gêne les autres
     
 ## Crée une branch 
    
 * Clique sur Git master puis New Branch