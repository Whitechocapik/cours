## Container

### Comment crée un conteneur

1 ) Un Conteneur `<div class="container"></div>`

Un `class="container"` va centrer la page 

 ```html
<div class="container" > 
    <div class="row"></div>
        <div class="col"></div>
        <div class="col"></div>
</div>
```

2 ) Un Conteneur Fluide `<div class="container-fluid"></div>`

Un `class="container-fluid"` va prendre toute la page

```html
<div class="container-fluid" > 
    <div class="row"></div>
        <div class="col"></div>
        <div class="col"></div>
</div>
```