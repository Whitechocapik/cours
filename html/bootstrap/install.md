# Installer Bootstrap
1)Installer Node.js

2)Aller sur le terminal

a) Pour créer le dossier init :  `npm init` 

package name : nomDuPackage
 
b) Pour installer Bootstrap :  `npm install bootstrap` 

3)Activer Bootstrap sur page HTML
```html
<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.css">
```
a) remonter d'un cran : `../`

# Utiliser Bootstrap

* Pour créer un tableau: `<div class="container"></div>`

* Pour créer une ligne : `<div class="row"></div>`

* Pour créer une colonne : `<div class="col"></div>`

* Ajouter un style pour modifier un colonne :`<div class="col leNomDeLaClass"></div>`



`
