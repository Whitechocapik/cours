# Row et Col
 
##  Faire un Tableau
un espace = `&nbsp;`
```html
<div class="container">
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col">&nbsp;</div>
                <div class="col">&nbsp;</div>
            </div>
        </div>        
        <div class="col">
                    <div class="row">
                        <div class="col">&nbsp;</div>
                        <div class="col">&nbsp;</div>
                    </div>
                </div>
    </div>    
</div>
``` 
## Offset

`col-offset` sert  à décaler des `col` de la `containe`

example : 

```html
<div class="container">
    <div class="row">
        <div class="col offset-2">&nbsp;</div>
    </div>
</div>
```


