# Structuration du HTML

```html
<!DOCTYPE HTML>
    <html>
        <head>
            <meta charset="UTF-8">
            <title></title>
        </head>
        <body>
        </body>
    </html>
```

# Element vide 


Un élément vide est un élément qui ne peut pas avoir de noeud enfant

## Exemple 

Exemple d'élément vide :

* `<img>` = permet de représenter une image
* `<meta>` = un élément qui inclut des attributs universels
* `<input>` =  est un élément utilisé pour créer un contrôle interactif dans un formulaire web
* `<br>` = crée un saut de ligne

## Les images


```html
<img src="" alt="">
```

`src=""` = permet d'indiquer le chemin vers notre image

`alt=""` = la description de l'image

image.png = pour les logos

image.jpg = pour les photos

image.gif = pour les animations

## Meta

```html
    <meta charset="UTF-8">
```

`charset=""` = permet de définir l'encodage de la page

# Elements imbriqués

## Exemple

### Element texte

titre et soustitre d'une page
```html
<h1></h1>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
```

`<strong></strong>` = mettre en **gras** 

`<em></em>` = mettre en *italique*

`<ul></ul>` = permet de représenter une liste non ordonné

`<li></li>` = liste item

`<ol></ol>` = represente une liste ordonnée

### Lien hypertexte

```html
    <a href=""></a>
```

`href=""` = Cet attribut indique la cible du lien  sous la  forme d'un URL

