
# Les Opérateur

Un opérateur est une fonction qui permet de manipuler un array

## forEach (pourChaque)

l'opérateur `forEach()` permet
 d'exercuter une fonction donnée  sur chaque élément du tableau 

```javascript
    const array1 = ['b','c','d'];

    array1.forEach(element => console.log(element));

    //expected output: "a"
    //expected output: "c"
    //expected output: "d"
    
```

## map 

la fonction `map()` crée un nouveau tableau avec les résultats de l'appel d'une fonction fournie sur chaque élément du tableau appelant.

```javascript
    const array1 = [1, 4, 9, 16];
    
    const map1 = array1.map(x => x + 2);

    console.log(map1);
    // Array [3, 6, 11, 18]
```

## filter

 la fonction `filter` filtre tous les éléments d'un tableau 

```javascript
    const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

    const filterArray = words.filter(word => word.length > 8);


    console.log(filterArray);
    // Array ["exuberant","destruction","present"]
```
## sort

La méthode `sort()` trie les éléments d'un tableau, dans ce même tableau, et renvoie le tableau. 
```javascript
    const tab = [40, 100, 1, 5, 25, 10];

    const sortArray = tab.sort((number1, number2) => number1 - number2);
    

    console.log(sortArray);
    // Array [1, 5, 10, 25, 10, 40, 100]
```
## push

la méthode `push()` ajoute un ou plusieurs éléments à la fin d'un tableau  et retourne la nouvelle taille du tableau
```javascript
    nomDuTableau.push(nomDeLObjet);
```
## concat

la méthode `concat()` est utilisée afin de fusionner un ou plusieurs tableaux en les concaténant.
```javascript
    const tab2 = [];
    const tab3 = [];
    
    const tab4 = [...tab2, ...tab3];
    console.table(tab4); 
```

## splice

la méthode `splice()` modifie le contenu d'un tableau en retirant des éléments et/ou
                      en ajoutant de nouveaux éléments à même le tableau
```javascript
    const spliceArray = array.splice(debut, nbASupprimer, element)
```
`debut` L'indice à partir duquel commencer le tableau.

`nbASupprimer` Un entier indiquant le nombre d'anciens élément à remplacer.

`élément` Les éléments à ajouter au tableau à partir de `debut`

## slice

la méthode `slice()` renvoie un objet tableau, contenant une copie
 
```javascript
    .slice();
    .slice(debut);
    .slice(debut, fin);
```
`debut` Indice depuis lequel commencer l'extraction
`fin` Indice auquel arrêter l'extraction.
## reduce

la méthode `reduce()` applique une fonction qui est un accumulateur et qui traite chaque valeur d'une liste
```javascript
 .reduce((accumulator, currentValue) => accumulator + currentValue ,0)

```
## some
la méthode `some` test si au moins un élément du tableau passe le test
```javascript
const array = [1, 2, 3, 4, 5];

const even = (element) => element % 2 === 0;

console.log(array.some(even));
//  true

```
## every

la méthode `every` permet de tester si tous les éléments d'un tableau 
vérifient une condition donnée 
```javascript
const isBelowThreshold = (currentValue) => currentValue < 40;

const array1 = [1, 30, 39, 29, 10, 13];

console.log(array1.every(isBelowThreshold));
// true
```

## values

`.values()` renvoie un tableau contenant les valeurs des propriétés

```javascript
const object1 = {
  a: 'somestring',
  b: 42,
  c: false
};

console.log(Object.values(object1));

// return Array ["somestring", 42, false]
```

## keys

`.keys()` renvoie un tableau contenant les noms des propriétés 

```javascript
const object1 = {
  a: 'somestring',
  b: 42,
  c: false
};

console.log(Object.values(object1));

// return Array ["a", "b", "c"]

```