# Les Fonctions

Le constructeur `Function` crée un nouvel objet Function. En JavaScript, chaque fonction est un objet Function.


## Syntaxe

```javascript
function boo(param1, param2) {
  return param1 + param2
};
```

## Addition & Soustraction


```javascript
function addition(param1, param2) {
    return param1 + param2;
}
const add =addition(param1, param2);
```
```javascript
function Soustraction(param1, param2) {
        return param1 - param2;
}
const sous =Soustraction(param1, param2);
```

## Arrow fonction

Les `Arrow function` permet d'avoir une syntaxe plus courte que les expressions de fonction

```javascript
hello = () => {
  return "Hello World!";
}
```