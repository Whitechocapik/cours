# Loops

## For

La boucle `for` est composée de trois expressions optionnelles séparées 
par des points-virgules et encadrées entre des parenthèses qui sont suivies par une instruction à exécuter dans la boucle

```javascript
for (let i = 0 ; i < 20 ;  i++){

  console.log('toto');
}
```
`i + 1` = `i++`

### Boucle Infinie

Pour tuer la  machine  d'une personne qu'on aime pas vraiment

```javascript
for (let i = 0 ; true ; i++ ) {
    console.log('toto' + i);
}
// return Infinie
``` 
 

