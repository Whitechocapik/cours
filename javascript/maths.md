# Maths 

## Math.random

`Math.random`renvoie un nombre flottant pseudo-aléatoire

```javascript
console.log(Math.random());

// return 0,635355
```

### Random entre 2 nombre

```javascript
function createRandomNumber(min, max) {
  return Math.floor(min + Math.random() * (max - min));
}

console.log(createRandomNumber(10, 15));
// return number random entre (10, 15)
```

## Math.round

`Math.round()` retourne la valeur d'un nombre arrondi à l'entier le plus proche

```javascript
console.log(Math.round(0.9));

// return : 1
```

## Math.floor

`Math.floor` renvoie le plus grand entier qui est inférieur ou égal à un nombre

```javascript
console.log(Math.floor(5.95));
// return : 5

console.log(Math.floor(-5.05));
// return : -6
```

## Math.toFixed

`.toFixed` permet de modifier un nombre en notation à point-fixe

```javascript
console.log(10.4245424.toFixed(3)); //Integer

// return 10.424 String

```


## Math.parseInt

`parseInt` analyse une chaîne de caractère fournie en argument et renvoie un entier

```javascript
console.log(parseInt('10')) // String

// return : 10 Integer
```
 

## Math.parseFloat

`parseFloat` permet de transformer une chaîne de caractère de nombre à virgule en un nombre flotant 

```javascript
console.log(parseFloat('2.3')) // String

// return : 2.3 Integer
```