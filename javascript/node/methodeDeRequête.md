# Méthode de requête HTTP

## Get

Les requêtes `GET` doivent uniquement être utilisées afin de récupérer des données.

```javascript
app.get('/api/hostels/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const findHostel = hostels.find((hotel) => hotel.id === id);
    return res.send(findHostel);
});
```

## Post

La méthode `POST` permet d'ajouter une entité à notre base de donnée.

```javascript
app.post('/api/hostel',
    (req, res) => {
        const newUser = req.body;
        const id = hostels.length + 1;
        hostels.push({...newUser, id});
        return res.send(hostels);
    });

```

## Put

La méthode `PUT` remplace toutes les représentations par le contenu de la requête.

```javascript
app.put('/api/hostels/:id',
    (req, res) => {
        const newUser = req.body;
        const id = parseInt(req.params.id);
        const index = hostels
            .findIndex((hotel) => hotel.id === id);
        hostels[index] = {...newUser, id};
        return res.send(hostels);
    });
    
```

## Delete

La méthode `DELETE` supprime la ressource indiquée.

```javascript
app.delete('/api/hostels/:id',
    (req, res) => {
        const id = parseInt(req.params.id);
        const deleteHostel = hostels.filter((hotel) => hotel.id !== id);
        return res.send(deleteHostel);
    });
```

## Patch

La méthode `PATCH` est utilisée pour modifier une partie de l'objet

```javascript
app.patch('/api/users/:id',
    (req, res) => {
        const newUser = req.body;
        const id = parseInt(req.params.id);
        const index = hostels
            .findIndex((hotel) => hotel.id === id);
        hostels[index] = {...hostels[index], ...newUser};
        return res.send(hostels);
    });
    
```