# Les Variables


## Var

L'instructeur `var` est une variable qui n'est plus utilisé

`var leNomDeLaVariable =`

## Const

L'instructeur `const` est une variable dont la valeur ne change pas

`const  leNomDeLaVariable =`

## Let

L'instructeur `let` permet de changer la valeur de la variable plusieur fois
 
`let leNomDeLaVariable =`