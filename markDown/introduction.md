# Comment Noter

## Les Titres et Sous-Titres

Titre : #

Sous-Titre : ##


## Comment Noter du Code

example 1:
`const toto = 4` 

example 2:

```javascript
const maVariable = 42
```
## Type d'Ecriture

Gras : **coucou**

Italique : _coucou_

Citation : 
>   > coucou
>
>   coucou

Lien : [coucou](coucou.com)
